package ru.nn.testslr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {

        Long start=System.currentTimeMillis();
        calculateFibonacci();
        System.out.println();
        long stop = System.currentTimeMillis();
        System.out.println("Calculation time: "+(stop-start)+" msec.");

        while (true) {
            isPalindrome();
        }
    }

    private static void isPalindrome() {
        int a,b,c;
        System.out.println("Enter number:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            a = Integer.parseInt(reader.readLine());
            b=0;
            c=a;
            while (c>0){
                b=b*10+c%10;
                c=c/10;
            }
            System.out.println(a==b?"The number is a palindrome":"The number isn't a palindrome");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void calculateFibonacci(){
        BigInteger n0 = new BigInteger("0");
        BigInteger n1 = new BigInteger("1");
        BigInteger n2;
        System.out.print(n1+" ");
        for(int i = 2; i <= 100; i++){
            n2=n0.add(n1);
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;
        }
    }
}
